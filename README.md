### Song Downloader

Simple tool to download mp3 song from one particular website

#### Compile

```console
cd mod/songdownloader/songdownloader
go build
```

#### Run

```console
cd mod/songdownloader/songdownloader
./songdownloader site0 <site0-baseurl>/music/a.r.rahman
```

This command will download all the songs from a.r.rahman available in site0 website. To know the baseurl, decrypt `sites.enc` file with following command

```console
openssl enc -d -aes-256-ctr -pbkdf2 -a -A -in sites.enc -out sites
```

This `sites.enc` file was created using this command

```console
openssl enc -e -aes-256-ctr -pbkdf2 -in sites -a -A -out sites.enc
```

password: this project's real password
