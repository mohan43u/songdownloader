package songdownloader

import (
	"fmt"
	"io"
	"log"
	"net/http"
	neturl "net/url"
	"os"
	"time"

	"golang.org/x/net/html"
)

type DownloadLink struct {
	name string
	url string
}

type Site0PageParser struct {
	tokenizer *html.Tokenizer
	logger *log.Logger
}

func NewSite0PageParser() *Site0PageParser {
	self := &Site0PageParser{}
	self.logger = log.Default()
	return self
}

func GetBaseUrl(url string, withpath bool) string {
	result := ""
	parsedurl, err := neturl.Parse(url)
	if err != nil {
		fmt.Print("Failed to parse url" + url)
		return result
	}
	if withpath == false {
		parsedurl.Path = ""
	}
	parsedurl.RawQuery = ""
	parsedurl.Fragment = ""
	result = parsedurl.String()
	return result
}

func (self *Site0PageParser) Download(downloadlink *DownloadLink) bool {
	if _, err := os.Stat(downloadlink.name); err == nil {
		self.logger.Print("file " +
			downloadlink.name + " already exists in current directory, skipping")
		return false
	}

	response, err := http.Get(downloadlink.url)
	if err != nil {
		self.logger.Print("failed to get " + downloadlink.url + ": " + err.Error())
		return false
	}

	data, err := io.ReadAll(response.Body)
	if err != nil {
		self.logger.Print("failed to read response: " + err.Error())
		return false
	}

	if err := os.WriteFile(downloadlink.name, data, 0644); err != nil {
		self.logger.Print("failed to write file: " + err.Error())
		return false
	}

	self.logger.Print(downloadlink.name + " " + downloadlink.url)
	return true
}

func (self *Site0PageParser) Parse(url string) string {
	aifound := false
	pnfound := false
	nexturl := ""

	response, err := http.Get(url)
	if err != nil {
		self.logger.Print("Failed to get " + url)
		return nexturl
	}
	url = GetBaseUrl(url, true)

	self.tokenizer = html.NewTokenizer(response.Body)
	loop := true
	for loop {
		switch self.tokenizer.Next() {
		case html.ErrorToken:
			if self.tokenizer.Err() != io.EOF {
				self.logger.Print("Error occurred during parsing: " +
					self.tokenizer.Err().Error())
			}
			loop = false
		case html.StartTagToken:
			token := self.tokenizer.Token()
			for _, attribute := range token.Attr {
				if attribute.Key == "class" &&
					attribute.Val == "a-i" {
					aifound = true
					break
				}
				if aifound &&
					token.Data == "a" &&
					attribute.Key == "href" {
					albumparser := NewSite0AlbumParser();
					downloadlink := albumparser.Parse(GetBaseUrl(url, false) +
						attribute.Val)
					aifound = false
					if downloadlink != nil {
						self.Download(downloadlink)
						time.Sleep(5 * time.Second)
					}
					break
				}

				if attribute.Key == "class" &&
					attribute.Val == "page next" {
					pnfound = true
					break
				}

				if pnfound &&
					token.Data == "a" &&
					attribute.Key == "href" {
					nexturl = GetBaseUrl(url, false) + attribute.Val
					pnfound = false;
					break
				}
			}
		}
	}
	return nexturl
}
