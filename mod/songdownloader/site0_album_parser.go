package songdownloader

import (
	"io"
	"log"
	neturl "net/url"
	"net/http"
	"strings"

	"golang.org/x/net/html"
)

type Site0AlbumParser struct {
	tokenizer *html.Tokenizer
	logger *log.Logger
}

func NewSite0AlbumParser() *Site0AlbumParser {
	self := &Site0AlbumParser{}
	self.logger = log.Default()
	return self
}

func (self *Site0AlbumParser) Parse(url string) *DownloadLink {
	var downloadlink *DownloadLink
	dlinkfound := false

	self.logger.Print(url)
	response, error := http.Get(url)
	if error != nil {
		self.logger.Print("Failed to get " + url)
		return nil
	}

	self.tokenizer = html.NewTokenizer(response.Body)
	loop := true
	for loop {
		switch self.tokenizer.Next() {
		case html.ErrorToken:
			if self.tokenizer.Err() != io.EOF {
				self.logger.Print("Error occurred during parsing: " +
					self.tokenizer.Err().Error())
			}
			loop = false
		case html.StartTagToken:
			token := self.tokenizer.Token()
			if token.Data == "a" {
				for _, attribute := range token.Attr {
					if attribute.Key == "class" &&
						attribute.Val == "dlink anim" {
						dlinkfound = true
					}
					if dlinkfound &&
						attribute.Key == "href" &&
						strings.Contains(attribute.Val, "zip320") {
						parsedurl, err := neturl.Parse(url)
						if err != nil {
							self.logger.Print("failed to parse url" + url)
							dlinkfound = false
							break
						}
						name := strings.Trim(parsedurl.Path, "/") + ".zip"
						url := GetBaseUrl(url, false) +	attribute.Val
						downloadlink = &DownloadLink{
							name: name,
							url: url,
						}
						dlinkfound = false
						break
					}
				}
			}
		}
		if downloadlink != nil {
			break
		}
	}
	return downloadlink
}
