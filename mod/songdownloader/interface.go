package songdownloader

type SongDownloader interface {
	Parse(url string) string
}
