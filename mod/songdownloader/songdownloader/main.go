package main

import (
	"fmt"
	"os"

	pkgsongdownloader "gitlab.com/mohan43u/songdownloader/mod/songdownloader"
)

func main() {
	if len(os.Args) < 3 {
		fmt.Println("[usage]" + os.Args[0] + " <site0|site1|..> <url>")
		return
	}

	var songdownloader pkgsongdownloader.SongDownloader
	switch os.Args[1] {
	case "site0":
		songdownloader = pkgsongdownloader.NewSite0PageParser()
	default:
		fmt.Println("invalid site " + os.Args[1])
		return
	}

	nexturl := os.Args[2]
	for {
		nexturl = songdownloader.Parse(nexturl)
		if len(nexturl) <= 0 {
			break
		}
	}
}
